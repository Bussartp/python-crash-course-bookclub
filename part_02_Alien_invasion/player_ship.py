# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     01MAR2020
# Project:  python-crash-course-bookclub
# Filename: player_ship.py
import os

import pygame
from pygame.sprite import Sprite


class PlayerShip(Sprite):
    """A class to manage the player ship."""

    def __init__(self, ai_game):
        """Initialize the player ship and set its starting position."""
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.screen_rect = ai_game.screen.get_rect()

        # Load the ship image and get its rectangle.
        ship_path = os.path.join("images", "player_ships", "Green_Ship_Space.png")
        self.image = pygame.image.load(ship_path)
        self.rect = self.image.get_rect()

        # Start each new ship at the bottom center of the screen.
        self.rect.midbottom = self.screen_rect.midbottom

        # Store a decimal value for the player ship's horizontal position.
        self.x = float(self.rect.x)

        # Movement flag
        self.moving_right = False
        self.moving_left = False

    def blitme(self):
        """Draw the player ship at its current location."""
        self.screen.blit(self.image, self.rect)

    def update(self):
        """Update the player ship's postition based on the moven flag."""
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.player_ship_speed
        if self.moving_left and self.rect.left > 0:
            self.x -= self.settings.player_ship_speed

        self.rect.x = self.x

    def center_ship(self):
        """Center the ship on the screen."""
        self.rect.midbottom = self.screen_rect.midbottom
        self.x = float(self.rect.x)
