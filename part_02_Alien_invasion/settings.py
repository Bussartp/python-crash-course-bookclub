# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     01MAR2020
# Project:  python-crash-course-bookclub
# Filename: settings.py


class Settings:
    """A class to store all of the settings for Alien Invasion."""

    def __init__(self):
        """Initialize the game's static settings."""
        # Screen settings.
        self.screen_width = 1200
        self.screen_height = 800
        self.bg_color = (230, 230, 230)

        # Ship settings.
        self.player_ship_limit = 3

        # Bullet settings.
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = (60, 60, 60)
        self.bullets_allowed = 3

        # Alien Settings
        self.fleet_drop_speed = 10

        # How quickly the game speeds up.
        self.speedup_scale = 1.1

        # How quickly the alien point values increase.
        self.score_scale = 1.5

        self.initialize_dynamic_settings()

    def initialize_dynamic_settings(self):
        """Initialize settings that can change thought the game."""
        self.player_ship_speed = 1.5
        self.bullet_speed = 1.5
        self.alien_speed = 1.0

        # Fleet_direction of 1 represents right; -1 represents left.
        self.fleet_direction = 1
        self.fleet_width = .2

        # Scoring
        self.alien_points = 50

    def increase_speed(self):
        """Increase the speed settings and the alien point values."""
        self.player_ship_speed *= self.speedup_scale
        self.bullet_speed *= self.speedup_scale
        self.alien_speed *= self.speedup_scale
        self.fleet_width += .2
        self.alien_points = int(self.alien_points * self.score_scale)
