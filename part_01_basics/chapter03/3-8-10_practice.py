# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     11FEB2020
# Project:  python-crash-course-bookclub
# Filename: 3-8-10_practice.py

locations = ['Thailand', 'Japan', 'Canada', 'Brazil']

print(locations)
print(sorted(locations))
print(locations)
locations.reverse()
print(locations)
locations.reverse()
print(locations)
locations.sort()
print(locations)
locations.reverse()
print(locations)

guests = ['Cleopatra', 'Alexander', 'Jesus']

print('\n{} guests.'.format(len(guests)))
