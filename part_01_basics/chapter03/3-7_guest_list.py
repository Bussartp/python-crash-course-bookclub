guests = ['Cleopatra', 'Alexander', 'Jesus']
message = ', you have been invited to dinner.'
print(guests[0] + message)
print(guests[1] + message)
print(guests[2] + message)

print('{} cannot make it to dinner.'.format(guests[2]))
guests[2] = 'Genghis Khan'

print(guests[0] + message)
print(guests[1] + message)
print(guests[2] + message)

print('Exciting news everyone!  I found a bigger table.')
guests.insert(0, 'Peter Pan')
guests.insert(2, 'Walt Disney')
guests.append('Braveheart')

print(guests[0] + message)
print(guests[1] + message)
print(guests[2] + message)
print(guests[3] + message)
print(guests[4] + message)
print(guests[5] + message)
print()

MESSAGE = 'Sorry {}, but you have been uninvited from dinner.'
print('I can only invited two people to dinner.')
print(MESSAGE.format(guests.pop()))
print(MESSAGE.format(guests.pop()))
print(MESSAGE.format(guests.pop()))
print(MESSAGE.format(guests.pop()))
message = ' you are still invited to dinner.'
print(guests[0] + message)
print(guests[1] + message)
del guests[0]
del guests[0]
print('Guests left: {}'.format(guests))
