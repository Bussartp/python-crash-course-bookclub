friends = ['Jake', 'Nathan', 'Ron', 'Norman']
print(friends[0])
print(friends[1])
print(friends[2])
print(friends[3])

print('Hello, my friend {}!'.format(friends[0]))
print('Hello, my friend {}!'.format(friends[1]))
print('Hello, my friend {}!'.format(friends[2]))
print('Hello, my friend {}!'.format(friends[3]))

print()
motorcycles = ['Honda', 'Zero', 'Ducati', 'Kawasaki']

print('{}s are very solid.'.format(motorcycles[0]))
print('{} makes iconic electric motorcycles!'.format(motorcycles[1]))
print('The fastest bikes are {}s!'.format(motorcycles[2]))
print('{} makes the Ninja; I always wanted one.'.format(motorcycles[3]))