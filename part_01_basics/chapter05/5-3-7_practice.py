# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     16FEB2020
# Project:  python-crash-course-bookclub
# Filename: 5-3-7_practice.py

alien_color = 'green'

if alien_color == 'green':
    print('Player earned 5 points.')

if alien_color == 'blue':
    print('Player earned 10 points.')

# If Else test
alien_color = 'blue'
if alien_color == 'green':
    print('Player earned 5 points.')
else:
    print('Player earned 10 points.')

alien_color = 'green'
if alien_color == 'green':
    print('Player earned 5 points.')
else:
    print('Player earned 10 points.')

# If elif Else test
alien_color = 'blue'
if alien_color == 'green':
    print('Player earned 5 points.')
elif alien_color == 'yellow':
    print('Player earned 10 points.')
elif alien_color == 'red':
    print('Player earned 15 points.')

alien_color = 'yellow'
if alien_color == 'green':
    print('Player earned 5 points.')
elif alien_color == 'yellow':
    print('Player earned 10 points.')
elif alien_color == 'red':
    print('Player earned 15 points.')

alien_color = 'red'
if alien_color == 'green':
    print('Player earned 5 points.')
elif alien_color == 'yellow':
    print('Player earned 10 points.')
elif alien_color == 'red':
    print('Player earned 15 points.')

age = 17

if age < 2:
    print('You are a baby.')
elif 2 <= age < 4:
    print('You are a toddler.')
elif 4 <= age < 13:
    print('You are a kid.')
elif 13 <= age < 20:
    print('You are a teenager.')
elif 20 <= age < 65:
    print('You are a adult.')
