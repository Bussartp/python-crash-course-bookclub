# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     16FEB2020
# Project:  python-crash-course-bookclub
# Filename: 5-1-2_practice.py

car = 'subaru'
print("Is car == 'subaru'? I predict True.")
print(car == 'subaru')

print("\nIs car == 'audi'? I predict False.")
print(car == 'audi')

print('\nRunning 10 test cases.  Should be 5 trues and then 5 false.')
car2 = 'SuBaRu'
# TRUE Tests
print(car.casefold() == car2.casefold())
print(car.capitalize() == car2.capitalize())
print(car.upper() == car2.upper())
print(len(car) == len(car2))
print(car != car2)

# FALSE Tests
print(car == car2)
print(car.capitalize() == car2.lower())
print(car.upper() == car2.lower())
print(len(car) != len(car2))
print(car == car2)

# Numerical Tests
print('\nNumeric Tests')
num1 = 21
num2 = 39

print(num1 == num2)
print(num1 != num2)
print(num1 < num2)
print(num1 > num2)
print(num1 >= num2)
print(num1 <= num2)

# AND and OR tests
print('\nAND and OR tests')

if car == car2.lower() or car == car2.upper():
    print('Upper or lower case test against car == true')

if car == car2.lower() and car == car2.upper():
    print('Upper or lower case test against car == true')

cars = ('subaru', 'bmw', 'tesla')
if car not in cars:
    print("car was not in cars.")
if car in cars:
    print("car was in cars.")
