# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     16FEB2020
# Project:  python-crash-course-bookclub
# Filename: 5-8-11_practice.py

user_names = ['jsnow01', 'kral02', 'spaceboy1', 'tangerine', 'fruitloop', 'admin']

for user_name in user_names:
    if user_name == 'admin':
        print('Hello admin, would you like to see a status report?')
    else:
        print('Hello {}, thank you for logging in.'.format(user_name))

print('\n5-9 Tests.')
user_names = []
if user_names:
    for user_name in user_names:
        if user_name == 'admin':
            print('Hello admin, would you like to see a status report?')
        else:
            print('Hello {}, thank you for logging in.'.format(user_name))
else:
    print("We need more users!")

print('\n5-10 Tests.')
current_users = ['jsnow01', 'kral02', 'spaceboy1', 'tangerine', 'fruitloop', 'admin', 'xxninjaxx']
new_users = ['pokemonmaster111', 'fruitloop', 'XxninjaxX', 'tigerblood', "Monkeyman"]

for new_user in new_users:
    if new_user.lower() in current_users:
        print('User name {} already exists, create a new user name.'.format(new_user))
    else:
        print('Congrats!  {} is available to use.'.format(new_user))
