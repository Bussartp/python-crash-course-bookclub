# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     22FEB2020
# Project:  python-crash-course-bookclub
# Filename: 7-1-3_practice.py


# car = input("What care would you like to rent?  ")
# print("Let's see if we can rent you a {}.".format(car))
#
# amount = int(input("How many people would you like to seat at your table?  "))
#
# if amount > 8:
#     print("You will have to wait for your table of {} people.".format(amount))
# else:
#     print("Your table for {} people is ready.".format(amount))

amount = int(input("Pass me a number and I will check if it is a multiple of 10.  "))
if amount%10 == 0:
    print("{} is a multiple of 10.".format(amount))
else:
    print("{} is not a multiple of 10.".format(amount))
