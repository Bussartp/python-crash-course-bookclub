# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     29FEB2020
# Project:  python-crash-course-bookclub
# Filename: 10-1-2_practice.py


filename = "resources\\learning_python.txt"

with open(filename) as file_object:
    # For loop style.
    # for line in file_object:
    #     print(line.strip())

    # Save lines so they can be worked externally.
    lines = file_object.readlines()

    for line in lines:
        print(line.strip())

# Write to the file and then close it.
with open("resources\\sample.txt", "w") as file_object:
    file_object.write("Test line from the code!.\n")
    file_object.write("Hello world!.\n")
# Use previously made file and print it's lines.
with open("resources\\sample.txt") as file_object:
    for line in file_object:
        print(line.strip())

# Try, except, else test

print("Try read a file given by the files variable.")
files = ["resources\\sampleFail1.txt", "resources\\sample.txt", "resources\\sampleFail2.txt"]
for file in files:
    try:
        # Use previously made file and print it's lines.
        with open(file) as file_object:
            for line in file_object:
                print(line.strip())
    except FileNotFoundError:
        # For a silent skip of the except use "pass".
        # pass
        print(f"{file} does not exist.  Skipping!")
