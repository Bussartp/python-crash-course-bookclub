# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     16FEB2020
# Project:  python-crash-course-bookclub
# Filename: 6-1-3_practice.py

person = {
    'first_name': 'sarah',
    'last_name': 'Smith',
    'age': 24,
    'city': 'Orlando'
}
for key, value in person.items():
    print(f"Key:\t{key}")
    print(f"Value:\t{value}")

print('\n6-2. Favorite Numbers:')
favorite_numbers = {
    'Sarah': 22,
    'Tammy': 3134,
    'Sally': 3,
    'Jake': 78,
}
for key, value in favorite_numbers.items():
    print(f"Key:\t{key}")
    print(f"Value:\t{value}")

print('\n6-3. Glossary:')
glossary = {
    'if': "Conditional statement.",
    'for': "Looping.",
    'elif': "Short hand for else if, preceded by an if statement.",
    'else': "Not requires, but final statement to the if stack..",
    '.lower': 'set the case of a string to lower case.  Good for literal comparisons.'
}
for key, value in glossary.items():
    print(f"Key:\t{key}")
    print(f"Value:\t{value}")
