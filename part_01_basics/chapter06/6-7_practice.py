# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     22FEB2020
# Project:  python-crash-course-bookclub
# Filename: 6-7-12_practice.py

person1 = {
    'first_name': 'sarah',
    'last_name': 'Smith',
    'age': 24,
    'city': 'Orlando'
}
person2 = {
    'first_name': 'Julio',
    'last_name': 'Perez',
    'age': 42,
    'city': 'Melbourne'
}
person3 = {
    'first_name': 'Randy',
    'last_name': 'Norb',
    'age': 22,
    'city': 'Orlando'
}
person4 = {
    'first_name': 'Jane',
    'last_name': 'Norb',
    'age': 23,
    'city': 'Orlando'
}
people = [person1, person2, person3, person4]

for person in people:
    for key, value in person.items():
        print("{}:\t{}".format(key, value))
    print("")

for person in people:
    for item in person:
        print("{}:\t{}".format(item.key, value))
    print("")