# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     29FEB2020
# Project:  python-crash-course-bookclub
# Filename: 9-1-8_practice.py
from part_01_basics.chapter09.classes.ice_cream_stand import IceCreamStand
from part_01_basics.chapter09.classes.restaurant import Restaurant

my_restaurant = Restaurant("Dommi", "Sushi")

my_restaurant.print_cuisine_type()
my_restaurant.print_name()
my_restaurant.open_restaurant()

my_ice_cream_stand = IceCreamStand("Dipperz", ["Strawberry", "Vanilla", "CHOCCY"])
my_ice_cream_stand.print_flavors()
my_ice_cream_stand.open_restaurant()
