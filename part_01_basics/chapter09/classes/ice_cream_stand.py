# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     29FEB2020
# Project:  python-crash-course-bookclub
# Filename: ice_cream_stand.py
from part_01_basics.chapter09.classes.restaurant import Restaurant


class IceCreamStand(Restaurant):

    def __init__(self, name, flavors):
        super().__init__(name, "Ice Cream")
        self.flavors = flavors

    def print_flavors(self):
        print(f"{self.name}'s serves these types of ice cream {self.flavors}.")
