# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     29FEB2020
# Project:  python-crash-course-bookclub
# Filename: restaurant.py


class Restaurant:
    """ A simple class to represent a Restaurant"""

    def __init__(self, name, cuisine_type):
        """ Init for Restaurant"""
        self.name = name.title()
        self.cuisine_type = cuisine_type

    def print_name(self):
        """ Prints the restaurant's name"""
        print(f"The restaurant's name is {self.name}.")

    def print_cuisine_type(self):
        """Prints the restaurant's cuisine type"""
        print(f"The restaurant's cuisine type is {self.cuisine_type}.")

    def open_restaurant(self):
        """Sets the restaurant to open"""
        print(f"The new restaurant, {self.name}, that serves {self.cuisine_type}, is now open!")
