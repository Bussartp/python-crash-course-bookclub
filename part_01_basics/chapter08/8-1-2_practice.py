# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     28FEB2020
# Project:  python-crash-course-bookclub
# Filename: 8-1-2_practice.py


def display_message():
    print("Hello world.")


display_message()


def favorite_book(book_name):
    print("One of my favorite books is {}.".format(book_name))


favorite_book("Python Crash Course.")
