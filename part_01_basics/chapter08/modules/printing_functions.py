# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     28FEB2020
# Project:  python-crash-course-bookclub
# Filename: printing_functions.py


def print_in_title(*names):
    """ Sets all names passed into it to Title case. """
    fixed_names = []
    for name in names:
        fixed_names.append(name.title())

    print(fixed_names)


def print_in_upper(*names):
    """ Sets all names passed into it to Upper case. """
    fixed_names = []
    for name in names:
        fixed_names.append(name.upper())

    print(fixed_names)
