# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     28FEB2020
# Project:  python-crash-course-bookclub
# Filename: 8-15_printing_models.py


import part_01_basics.chapter08.modules.printing_functions as pf

pf.print_in_title("James", "jessie", "meowth")
pf.print_in_upper("James", "jessie", "meowth")
