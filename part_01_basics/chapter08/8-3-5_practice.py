# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     28FEB2020
# Project:  python-crash-course-bookclub
# Filename: 8-3-5_practice.py


def make_shirt(size="Large", message="I love python!"):
    print("Made a size {} shirt.  Message to be printed the shirt: \"{}\"".format(size, message))


make_shirt("X-Large", "This monkey has an ugly face.")
make_shirt()
