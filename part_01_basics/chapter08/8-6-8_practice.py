# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     28FEB2020
# Project:  python-crash-course-bookclub
# Filename: 8-6-8_practice.py


def city_name(city, country):
    print("{}, {}".format(city, country))


city_name("Santiago", "chile")
city_name("Aurora", "USA")
city_name("paris", "France")


def make_album(artist, title, number_of_songs=""):
    if number_of_songs == "":
        return {'Artist': artist, 'Title': title}
    else:
        return {'Artist': artist, 'Title': title, 'Number Of Songs': number_of_songs}


print(make_album("Doggo", "Big ol Bone"))
print(make_album("Candy Cane Man", "Sweet and SPICY", 902))

while True:
    print("\n\nLet's make some albums! Insert \"q\" anytime to quit")
    artist_name = input("Provide an Artist: ")
    if artist_name == "q":
        break
    title_name = input("Provide an Title: ")
    if title_name == "q":
        break
    songs = input("Provide an amount of songs: ")
    if songs == "q":
        break
    print(make_album(artist_name, title_name, songs))

# Other Notes
"""This is a new comment style called triple quotes."""
"""
    Yay
    multi
    line
    comments!
"""

# Arbitrary arguments
# def function_name(*people)
# creates an empty list for names.
# Then just loop through the people.

# Arbitrary arguments v2
# def function_name(**people)
# creates an empty dictionary for people.

# Create a copy of a list so that the original does not get modified
# function_name(people[:])
