# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     11FEB2020
# Project:  python-crash-course-bookclub
# Filename: 4-10-12_practice.py


pizzas = ['Pepperoni', 'Sausage', 'Cheese', 'Ham', 'Mushroom']
print('The first 3 items of the list are: {}'.format(pizzas[:3]))
print('The middle 3 items of the list are: {}'.format(pizzas[1:4]))
print('The middle 3 items of the list are: {}'.format(pizzas[-3:]))

friend_pizzas = pizzas[:]
pizzas.append('Hamburger')
friend_pizzas.append('Pineapple')
print('My favorite pizzas are: ')
print(pizzas)
print("My friend's favorite pizzas are: ")
print(friend_pizzas)
