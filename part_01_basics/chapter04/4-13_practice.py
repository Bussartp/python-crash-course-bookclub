# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     16FEB2020
# Project:  python-crash-course-bookclub
# Filename: 4-13_practice.py

foods = ('Candy', 'Ice Cream', 'Salad', 'BBQ', 'Nachos')
print('Menu:')
for food in foods:
    print(food)

# foods[3] = 'Unicorn meat'
# Throws Error
# Traceback (most recent call last):
# File "D:/workspace/python-crash-course-bookclub/chapter04/4-13_practice.py", line 12, in <module>
# foods[3] = 'Unicorn meat'
# TypeError: 'tuple' object does not support item assignment

foods = ('Cajun', 'Ice Cream', 'Salad', 'BBQ', 'Italian')
print('\nRevised Menu:')
for food in foods:
    print(food)
