# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     11FEB2020
# Project:  python-crash-course-bookclub
# Filename: 4-1-2_practice.py

pizzas = ['Pepperoni', 'Sausage', 'Cheese']

for pizza in pizzas:
    print('I like {} pizza.'.format(pizza))
print('\nI really love pizza!')

animals = ['Dolphin', 'Kangaroo', 'Badger']

for animal in animals:
    print('A {} would make a cool pet!'.format(animal))
print('\nAny of these animals would make a cool pet!')
