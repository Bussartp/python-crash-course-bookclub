# Author:   Phillip J. Bussart
# Linkedin: https://www.linkedin.com/in/phillipbussart/
# Date:     11FEB2020
# Project:  python-crash-course-bookclub
# Filename: 4-3-9_practice.py

# Commented out unnecessary work.
# for number in range(1,21):
#     print(number)

# numbers = list(range(1, 1_000_001))
#
# for number in numbers:
#     print(number)

# numbers = list(range(1, 21, 2))
# print(numbers)

# threes = [value*3 for value in range(3, 31)]
# for three in threes:
#     print(three)

# threes = [value**3 for value in range(1, 11)]
# for three in threes:
#     print(three)
