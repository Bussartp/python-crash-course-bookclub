print(2+6)
print(11-3)
print(48/6)
print(2*4)
# Ints will default to float if there is a float involved and with division.

favorite_number = 7
message = f"My favorite number is {favorite_number}."
print(message)
