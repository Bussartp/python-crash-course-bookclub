first_name = "eRic"
message = f"Hello, {first_name.title()}!"
print(message)

message = f"Hello, {first_name.title()} {first_name.lower()} {first_name.upper()}!"
print(message)

quote = 'Linus Benedict Torvalds once said, "Talk is cheap. Show me the code."'
print(quote)

author = 'Linus Benedict Torvalds'
quote = '"Talk is cheap. Show me the code."'
message = '{} once said, {}. v2'.format(author, quote)
print(message)

first_name = " \n\tLinus "
print(first_name)
print(first_name.rstrip())
print(first_name.lstrip())
print(first_name.strip())




