first_name = 'ada'
last_name = 'lovelace'
full_name = f'{first_name} {last_name}'
print(full_name)
# This is called a f-string.  f is for format.
message = f'Hello, {full_name.title()}!'

print(message)

full_name = "{} {}".format(first_name, last_name)
print(full_name)

# \t = tab
# \n = new line

# .strip() does not permanently alter a value.
# .rstrip() right side strip of white space.
# .lstrip() left side strip of white space.


